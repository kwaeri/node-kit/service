/**
 * SPDX-PackageName: kwaeri/service
 * SPDX-PackageVersion: 0.10.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES
//import { ServiceProvider } from './src/service.mjs';


// ESM WRAPPER
export {
    SERVICE_TYPES,
    BaseService,
    BaseServiceProvider,
    Service,
    ServiceProvider,
    ExampleServiceProvider
} from './src/service.mjs';

export type {
    ServiceType,
    ServiceEventPromise,
    ServiceEventBits,
    ServicePromiseBits,
    ServiceProviderSubscriptions,
    ServiceProviderHelpText,
} from './src/service.mjs';

// DEFAULT EXPORT
//export default ServiceProvider;

